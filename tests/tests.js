QUnit.module( "Seeds" );
QUnit.test( "Check 3 valid seeds", function( assert ) {
  $("body").append("<input id='privKey' value='shmUdaDgSu5Ga2TUBgRKNskrr74Ga' style='display:none;'>");
  assert.ok( app.checkPrivKey() == 1, "Passed!" );
  $("#privKey").remove();

  $("body").append("<input id='privKey' value='sarfxHcivLPHhY1gUKi8RjUX5EXCc' style='display:none;'>");
  assert.ok( app.checkPrivKey() == 1, "Passed!" );
  $("#privKey").remove();

  $("body").append("<input id='privKey' value='shHJiSBCAjMDnGZ3aLaanmJoBTnXP' style='display:none;'>");
  assert.ok( app.checkPrivKey() == 1, "Passed!" );
  $("#privKey").remove();

});
QUnit.test( "Check 3 _invalid_ seeds", function( assert ) {
  $("body").append("<input id='privKey' value='shmUdaDgSu5Ga2TUBgRKNdkrr74Ga' style='display:none;'>");
  assert.ok( app.checkPrivKey() != 1, "Passed!" );
  $("#privKey").remove();

  $("body").append("<input id='privKey' value='shmUdXDgSu5Ga2TUBgRKNdkrr74Ga' style='display:none;'>");
  assert.ok( app.checkPrivKey() != 1, "Passed!" );
  $("#privKey").remove();

  $("body").append("<input id='privKey' value='shmUdaDgKu5Ga2TUBgRKNdkrr74Ga' style='display:none;'>");
  assert.ok( app.checkPrivKey() != 1, "Passed!" );
  $("#privKey").remove();

  $(".messenger").remove();
});

QUnit.module( "SHA-256 functions" );
QUnit.test( "Check 5 valid hash digests", function( assert ) {
	 assert.ok( sha256("") == "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855", "Passed!" );
	 assert.ok( sha256("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855") == "cd372fb85148700fa88095e3492d3f9f5beb43e555e5ff26d95f5a6adc36f8e6", "Passed!" );
	 assert.ok( sha256("Lorem ipsum dolorem sit amet") == "50f398f036ec608f52fd34c6fc88368f7ae16d274b9bda6edfd1eff73839938d", "Passed!" );
	 assert.ok( sha256("<p>Text.</p>$$") == "0cb576ca90ce55273c016796468925df5dbbcc9fad18dfe57ebacb88f02dafea", "Passed!" );
	 assert.ok( sha256("..//??^#@µ˜ª∆∞ƒ®<p>Text.</p>$$") == "2ad05f88153d2d39acca25be20ccf47b3c5edc983b60956eb9108ea59ec9a46d", "Passed!" );
});
QUnit.test( "Check  2 invalid hash digests", function( assert ) {
	 assert.ok( sha256("") != "2ad05f88153d2d39acca25be20ccf47b3c5edc983b60956eb9108ea59ec9a46d", "Passed!" );
	 assert.ok( sha256("abc") != "2ad05f88153d2d39acca25be20ccf47b3c5edc983b60956eb9108ea59ec9a46d", "Passed!" );
});

QUnit.module( "SHA-512 functions" );
QUnit.test( "Check 5 valid hash digests", function( assert ) {
	assert.ok( sha512("") == "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e", "Passed!" );
	assert.ok( sha512("cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e") == "8fb29448faee18b656030e8f5a8b9e9a695900f36a3b7d7ebb0d9d51e06c8569d81a55e39b481cf50546d697e7bde1715aa6badede8ddc801c739777be77f166", "Passed!" );
	assert.ok( sha512("8fb29448faee18b656030e8f5a8b9e9a695900f36a3b7d7ebb0d9d51e06c8569d81a55e39b481cf50546d697e7bde1715aa6badede8ddc801c739777be77f166") == "111b55a07597cabe04996109644c0d119175f935c42d254eea56b0b0360fa630ef5f4344512827117f9020dd3d8277ce4efc9ad7dad075d99813ec50896582d2", "Passed!" );
	assert.ok( sha512("..//??^#@µ˜ª∆∞ƒ®<p>Text.</p>$$") == "0fb5391d8358ba69b6e154d7e8af5241aab1bb5b1d876d1cfb0d96d6044d0f1a2de90cefc4ad39b2cf2b6f1422b490c6eaaa06332d860878b0e476fcd37482d8")
	assert.ok( sha512("<p>Text.</p>$$") == "cdb7bca1e4471a73468224e91b89ca9b8181b97cf7d2376d52b2de558d60f2d1ed860c5646ee7b9857033ea6f1cdfebba5d657a5f1eace99cd019e9a9276fd93")
});

QUnit.test( "Check 2 invalid hash digests", function( assert ) {
	assert.ok( sha512("") != "Af83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e", "Passed!" );
	assert.ok( sha512("cf83e1357eefb8bdf1542850d66d8007d620e4050b5715ds83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e") != "8fb29448faee18b656030e8f5a8b9e9a695900f36a3b7d7ebb0d9d51e06c8569d81a55e39b481cf50546d697e7bde1715aa6badede8ddc801c739777be77f166", "Passed!" );
});

QUnit.module( "Generate a public/private key from seeds" );
QUnit.test( "Test with a valid seed", function( assert ) {
		 var key1 = keypairs.deriveKeypair("sarfxHcivLPHhY1gUKi8RjUX5EXCc");
		 assert.ok( key1.privateKey == "002C69BA3461A8335BD45F1B2D668D2B7D8996C1F2F68F4DBCB8D44DA827D8B845", "Passed!" );
		 assert.ok( key1.publicKey == "029CB41C03E7A446589EF5ED741C4AB1A30ADBD6DEDB709CAC57386B0AD3AC9EB4", "Passed!" );
});

QUnit.test( "Test with a valid seed", function( assert ) {
		 var key2 = keypairs.deriveKeypair("shmUdaDgSu5Ga2TUBgRKNskrr74Ga");
		 assert.ok( key2.privateKey == "00D186633C44AA181D9FA4494F173EFE40E8EFD319ABB313CCE3772D4CE8882496", "Passed!" );
		 assert.ok( key2.publicKey == "03EC24C21DB6BC8BE6ADD4934D7A3944B30130538B805567382E747B808E2CB64B", "Passed!" );
});

QUnit.test( "Test with valid seed", function( assert ) {
		 var key3 = keypairs.deriveKeypair("shHJiSBCAjMDnGZ3aLaanmJoBTnXP");
		 assert.ok( key3.privateKey == "00B4C2F12323445A0068984820050B88569060E5C206FD23DC7DFD92B03AD02913", "Passed!" );
		 assert.ok( key3.publicKey == "03F63FE965DF1574DC96CE97E8E68D43CCA9C75BD85FCC7BD63330FC2DA3DD40C7", "Passed!" );
});


QUnit.module( "Derive public address from public key" );
QUnit.test( "Test with a publicKey", function( assert ) {
		 var pubAddr = keypairs.deriveAddress("029CB41C03E7A446589EF5ED741C4AB1A30ADBD6DEDB709CAC57386B0AD3AC9EB4")
		 assert.ok( pubAddr == "r3JHubNpU5m1y94eX8x3JiFbA4xv1WVkHL", "Passed!" );
});

QUnit.test( "Test with a publicKey", function( assert ) {
		 var pubAddr = keypairs.deriveAddress("03EC24C21DB6BC8BE6ADD4934D7A3944B30130538B805567382E747B808E2CB64B")
		 assert.ok( pubAddr == "rUZ9Lhz3rbVPsmAtG3cShiRQPHQnWdTB7D", "Passed!" );
});

QUnit.test( "Test with a publicKey", function( assert ) {
		 var pubAddr = keypairs.deriveAddress("03F63FE965DF1574DC96CE97E8E68D43CCA9C75BD85FCC7BD63330FC2DA3DD40C7")
		 assert.ok( pubAddr == "rEUrQ8iz352ZgpiZFmzcFZiESQQ9ju8kS8", "Passed!" );
});

QUnit.module( "Score password complexity" );
QUnit.test( "Test with a complex password", function( assert ) {
  $("body").append("<input id='passwordDOM' value='shmUdaDgSu5Ga2TUBgRKNskrr74Ga' style='display:none;'>");
  assert.ok( app.scorePassword() > 90, "Passed!" );
  $("#passwordDOM").remove();
});

QUnit.test( "Test with a complex password", function( assert ) {
  $("body").append("<input id='passwordDOM' value='js8Hnsbsha-a9s8shsns.ssjs' style='display:none;'>");
  assert.ok( app.scorePassword() > 90, "Passed!" );
  $("#passwordDOM").remove();
});

QUnit.test( "Test with a complex password", function( assert ) {
  $("body").append("<input id='passwordDOM' value='js8Hnsbsha-a9s-a' style='display:none;'>");
  assert.ok( app.scorePassword() > 90, "Passed!" );
  $("#passwordDOM").remove();
});

QUnit.test( "Test with a easy password", function( assert ) {
  $("body").append("<input id='passwordDOM' value='a.a.a.a.a.a.a' style='display:none;'>");
  assert.ok( app.scorePassword() < 90, "Passed!" );
  $("#passwordDOM").remove();
});

QUnit.module( "Generate a valid transaction signature" );
QUnit.test( "Sign the JSON", function( assert ) {
 	xrpl = new ripple.RippleAPI();
 	var txJSON = '{"Flags":2147483648,"TransactionType":"AccountSet","Account":"r9cZA1mLK5R5Am25ArfXFmqgNwjZgnfk59","Domain":"726970706C652E636F6D","LastLedgerSequence":8820051,"Fee":"12","Sequence":23}';
 	var secret = 'shsWGZcmZz6YsWWmcnpfr6fLTdtFV';
	var keypair = { privateKey: '00ACCD3309DB14D1A4FC9B1DAE608031F4408C85C73EE05E035B7DC8B25840107A', publicKey: '02F89EAEC7667B30F33D0687BBA86C3FE2A08CCA40A9186C5BDE2DAA6FA97A37D8' };
	var res = xrpl.sign(txJSON, secret);

	assert.ok( res.signedTransaction == "12000322800000002400000017201B0086955368400000000000000C732102F89EAEC7667B30F33D0687BBA86C3FE2A08CCA40A9186C5BDE2DAA6FA97A37D874473045022100BDE09A1F6670403F341C21A77CF35BA47E45CDE974096E1AA5FC39811D8269E702203D60291B9A27F1DCABA9CF5DED307B4F23223E0B6F156991DB601DFB9C41CE1C770A726970706C652E636F6D81145E7B112523F68D2F5E879DB4EAC51C6698A69304", "Passed!" );

});
QUnit.test( "Sign the JSON", function( assert ) {
 	xrpl = new ripple.RippleAPI();
 	var txJSON = '{"Flags":2147483648,"TransactionType":"AccountSet","Account":"rUZ9Lhz3rbVPsmAtG3cShiRQPHQnWdTB7D","Domain":"","LastLedgerSequence":8820051,"Fee":"12","Sequence":24}';
 	var secret = 'shmUdaDgSu5Ga2TUBgRKNskrr74Ga';
	var keypair = { privateKey: '00D186633C44AA181D9FA4494F173EFE40E8EFD319ABB313CCE3772D4CE8882496', publicKey: '03EC24C21DB6BC8BE6ADD4934D7A3944B30130538B805567382E747B808E2CB64B' };
	var res = xrpl.sign(txJSON, secret); 

	assert.ok( res.signedTransaction == "12000322800000002400000018201B0086955368400000000000000C732103EC24C21DB6BC8BE6ADD4934D7A3944B30130538B805567382E747B808E2CB64B7446304402200145397ED837BD8E611DDBA8C202D78AFF2363433C07BF97615ADBE1FE10536D02202C0B1B87CF41B406783B3A53FF8EA6638876368F07C9583539962B22BAEF07B4770081147EBDA59BAA5BF22B7B21FF2E82420DDB5B4CCF34", "Passed!" );

});

QUnit.test( "Sign the JSON", function( assert ) {
 	xrpl = new ripple.RippleAPI();
 	var txJSON = '{"TransactionType":"AccountSet","Account":"rUZ9Lhz3rbVPsmAtG3cShiRQPHQnWdTB7D","Domain":"","LastLedgerSequence":10000051,"Fee":"12","Sequence":1029}';
 	var secret = 'shmUdaDgSu5Ga2TUBgRKNskrr74Ga';
	var keypair = { privateKey: '00D186633C44AA181D9FA4494F173EFE40E8EFD319ABB313CCE3772D4CE8882496', publicKey: '03EC24C21DB6BC8BE6ADD4934D7A3944B30130538B805567382E747B808E2CB64B' };
	var res = xrpl.sign(txJSON, secret);

	assert.ok( res.signedTransaction == "1200032400000405201B009896B368400000000000000C732103EC24C21DB6BC8BE6ADD4934D7A3944B30130538B805567382E747B808E2CB64B744630440220711C698B80BF2CC563A31C53E5A7645CBE25506FEFF30345B3F46991582AAB5602204C08E5EAF118406CA44961F9A6D4B91B7D0EE893502276CAEE34D67AFC0F8873770081147EBDA59BAA5BF22B7B21FF2E82420DDB5B4CCF34", "Passed!" );

});

QUnit.module( "Encrypt and decrypt with AES-CTR" );
QUnit.test( "Encrypt and decrypt a string, then compare the result ", function( assert ) {
 	 var textBytes = aesjs.utils.utf8.toBytes("This is a test");
     var key = aesjs.utils.hex.toBytes(sha256("NotSoVeryLongAndComplexPassword"));
     var aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(6));
     var encryptedBytes = aesCtr.encrypt(textBytes);
     var encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);
	 assert.ok( encryptedHex=="5bcaf492d826eed3d0de6762f4a8", "Passed!" );

	  var encryptedBytes = aesjs.utils.hex.toBytes("5bcaf492d826eed3d0de6762f4a8");
      var aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(parseInt(6)));
      var decryptedBytes = aesCtr.decrypt(encryptedBytes);
      var result = aesjs.utils.utf8.fromBytes(decryptedBytes);
      assert.ok( result=="This is a test", "Passed!" );
});

QUnit.test( "Encrypt and decrypt a string with special characters ", function( assert ) {
 	 var textBytes = aesjs.utils.utf8.toBytes("A str1ng with s0m€ st@ng& Càràcèr$ ?");
     var key = aesjs.utils.hex.toBytes(sha256("NotSoVeryLongAndComplexPassword"));
     var aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(6));
     var encryptedBytes = aesCtr.encrypt(textBytes);
     var encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);
	 assert.ok( encryptedHex=="4e82ee958a7ef39491897a73effc8e01757d75fbde30241c8c2d27eff727788e6d6f2d5fb1404048a6", "Passed!" );

	  var encryptedBytes = aesjs.utils.hex.toBytes("4e82ee958a7ef39491897a73effc8e01757d75fbde30241c8c2d27eff727788e6d6f2d5fb1404048a6");
      var aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(parseInt(6)));
      var decryptedBytes = aesCtr.decrypt(encryptedBytes);
      var result = aesjs.utils.utf8.fromBytes(decryptedBytes);
      assert.ok( result=="A str1ng with s0m€ st@ng& Càràcèr$ ?", "Passed!" );
});


