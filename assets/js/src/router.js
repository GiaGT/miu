var router = function() {

	var loadPage = function(page, bindings)
    {
        $("#main-content").empty();
        $("#main-content").load("views/"+page, null, bindings);
    }

     var routes = {
     	'/': { on: function() 
	     		{ 
	     			loadPage("main.html", function() {
		     				 	$("#testSettingsButton").on('click', function() {
						        $("#serverAddress").val("s.altnet.rippletest.net:51233");
						        $("#privKey").val("shmUdaDgSu5Ga2TUBgRKNskrr74Ga");
					        });

					        $("#checkAndConnectButton").on('click', function() {
					                app.checkServerConnection();
					        });
					}); 
	     		} 
	     	 },

	     '/infoStatus': { on: function() 
	     		{ 
	     			checkConnection();
	     			loadPage("infoStatus.html", function() { app.infoStatus(); }); 
	     		} 
	     	 },

	      '/destinationAddress': { on: function() 
	     		{ 
	     			checkConnection();
	     			loadPage("destinationAddress.html", function() { 
	     					$("#testDestinationAddress").on('click', function() {
	     						$("#destinationAddress").val("rsLpY1k3PYNfLv9ggUp5e6j25P6EWYiQHF");
	     					})

	     					$("#checkDestinationAddressButton").on('click', function() {
	     						app.checkDestinationAddress();
	     					})
	     			 }); 
	     		} 
	     	 },

	     	 '/upload': { on: function() 
	     		{ 
	     			checkConnection();
	     			loadPage("upload.html", function() { app.upload(); }); 
	     		} 
	     	 },

	     	  '/message': { on: function() 
	     		{ 
	     			checkConnection();
	     			loadPage("message.html", function() { }); 
	     		} 
	     	 },

	     	  '/send': { on: function() 
	     		{ 
	     			checkConnection();
	     			loadPage("send.html", function() { app.sendInit(); }); 
	     		} 
	     	 },

	     	 '/result': { on: function() 
	     		{ 
	     			checkConnection();
	     			loadPage("result.html", function() { app.loadResults(); }); 
	     		} 
	     	 },

	     	  '/search': { on: function() 
	     		{ 
	     			checkConnection();
	     			loadPage("search.html", function() { app.searchInit(); }); 
	     		} 
	     	 },
	     	  '/searchResult': { on: function() 
	     		{ 
	     			checkConnection();
	     			loadPage("searchResult.html", function() { app.searchResult(); }); 
	     		} 
	     	 },
	     	  '/hash': { on: function() 
	     		{ 
	     			loadPage("hash.html", function() { }); 
	     		} 
	     	 },
	     	  '/decrypt': { on: function() 
	     		{ 
	     			loadPage("decrypt.html", function() { }); 
	     		} 
	     	 },

     }

      var router = Router(routes).configure({
          notfound: function() {
              document.location = "#/";
          }
      });

      var checkConnection = function() {
      	if(app.getConnectionStatus() == 0) {
				//window.location.href = "#/";
				return 0;
			}
      }

		var initialize = function() {
			router.init(['/']);
		}     
   

    return {
    	initialize: initialize,
    };

}();

