# LedgerCert

Certify the existence of a document or save data on a public blockchain without intermediaries.

## Marconi International University - Capstone Project

This is the capstone project of Giacomo M. Galatone for the Bachelor in Computer Engineering at Marconi International University. 

## Getting Started

Download or clone this repository, uncompress it and serve it from a webserver. Alternatively you can use an online version of LedgerCert at  [https://giagt.bitbucket.io/](https://giagt.bitbucket.io/)

If you use Linux or MacOS you can serve it with one of the following commands:

```
python -m SimpleHTTPServer 8000
php -S 127.0.0.1:8000
```

If you use Windows you can download and install Apache, IIS or use IIS Express.

```
C:\> "C:\Program Files (x86)\IIS Express\iisexpress.exe" /path:C:\thisDir /port:8000
```

## Hints

**NEVER** use this software on the LIVE NETWORK, you may loose your funds! Always use it in the test network.
To test the software you can use the "Use test settings" button.

## Tests

The unit tests are available in the tests/ directory and at the URL [https://giagt.bitbucket.io/tests/](https://giagt.bitbucket.io/tests/)

## Authors

* **Giacomo M. Galatone** - *Design and development* - [giagtnet@gmail.com](mailto:giagtnet@gmail.com).

## License

This project is licensed under the GPLv2 license.




